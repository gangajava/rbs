package com.rbs.primenumbers.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice

public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = { NoPrimeNumbersException.class})
    protected ResponseEntity<Object> handleNoAccountsException(NoPrimeNumbersException ex, WebRequest request) {
        String bodyOfResponse = "There are no prime numbers up to given number";
        return handleExceptionInternal(ex, bodyOfResponse,
                        new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(value = { NotValidInputException.class})
    protected ResponseEntity<Object> handleNoSuchAccountException(NotValidInputException ex, WebRequest request) {
        String bodyOfResponse = "The input should be greater than zero";
        return handleExceptionInternal(ex, bodyOfResponse,
                        new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
}
