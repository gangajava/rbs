package com.rbs.primenumbers.service;

import com.rbs.primenumbers.model.PrimeNumbers;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Component;

@Component
public class PrimeNumbersCache {
    private final Map<Long, PrimeNumbers> allPrimess = new HashMap<>();
    public PrimeNumbersCache() {

    }

    public PrimeNumbers getPrimeNumbersFronCache(long cacheKey) {
        if(allPrimess.containsKey(cacheKey)) {
            return allPrimess.get(cacheKey);
        }
        return null;
    }

    public void addPrimeNumbersToCache(long key, PrimeNumbers primeNumbers) {
        allPrimess.put(key, primeNumbers);
    }


}
