package com.rbs.primenumbers.service;

import com.rbs.primenumbers.model.PrimeNumbers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PrimeNumbersService {

    private PrimeNumbersCache primeNumbersCache;

    @Autowired
    public PrimeNumbersService(PrimeNumbersCache primeNumbersCache) {
        this.primeNumbersCache = primeNumbersCache;
    }
    public PrimeNumbers getAllPrimeNumbers(long number) {
       PrimeNumbers primes = primeNumbersCache.getPrimeNumbersFronCache(number);
       if(primes == null) {
           primes = findPrimeNumbers(number);
           primeNumbersCache.addPrimeNumbersToCache(number, primes);
       }
       return primes;
    }

    private PrimeNumbers findPrimeNumbers(long number) {
        PrimeNumbers primeNumbers = new PrimeNumbers();
        primeNumbers.setInitial(number);
        int i =0;
        int num =0;
        for (i = 1; i <= number; i++)
        {
            int counter=0;
            for(num =i; num>=1; num--)
            {
                if(i%num==0)
                {
                    counter = counter + 1;
                }
            }
            if (counter ==2)
            {
                //Appended the Prime number to the String
                primeNumbers.getPrimes().add(new Long(i));
            }
        }
        return primeNumbers;
    }
}
