package com.rbs.primenumbers.model;

import java.util.ArrayList;
import java.util.List;

public class PrimeNumbers {
    private long initial;
    private List<Long> primes = new ArrayList();

    public long getInitial() {
        return initial;
    }

    public void setInitial(long initial) {
        this.initial = initial;
    }

    public List<Long> getPrimes() {
        return primes;
    }

    public void setPrimes(List<Long> primes) {
        this.primes = primes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PrimeNumbers that = (PrimeNumbers) o;

        if (initial != that.initial) return false;
        return primes != null ? primes.equals(that.primes) : that.primes == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (initial ^ (initial >>> 32));
        result = 31 * result + (primes != null ? primes.hashCode() : 0);
        return result;
    }
}
