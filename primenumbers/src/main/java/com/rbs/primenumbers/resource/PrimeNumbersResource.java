package com.rbs.primenumbers.resource;

import com.rbs.primenumbers.exception.NoPrimeNumbersException;
import com.rbs.primenumbers.exception.NotValidInputException;
import com.rbs.primenumbers.model.PrimeNumbers;
import com.rbs.primenumbers.service.PrimeNumbersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/primes")
public class PrimeNumbersResource {

    private PrimeNumbersService primeNumbersService;

    @Autowired
    public PrimeNumbersResource(PrimeNumbersService primeNumbersService) {
        this.primeNumbersService = primeNumbersService;
    }
    @RequestMapping(value = "/{number}", method = RequestMethod.GET)
    public HttpEntity<PrimeNumbers> getPrimeNumbers(@PathVariable("number") long number ){
        if (number <= 0) {
            throw new NotValidInputException();
        } else if (number == 1L) {
            throw new NoPrimeNumbersException();
        }
        PrimeNumbers primeNumbers = primeNumbersService.getAllPrimeNumbers(number);
        return new ResponseEntity<>(primeNumbers, HttpStatus.OK);
    }

}
