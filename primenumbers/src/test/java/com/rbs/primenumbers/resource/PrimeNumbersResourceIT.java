package com.rbs.primenumbers.resource;

import com.rbs.primenumbers.model.PrimeNumbers;
import org.json.JSONException;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

public class PrimeNumbersResourceIT {
    @LocalServerPort
    private int port;

    TestRestTemplate restTemplate = new TestRestTemplate();

    HttpHeaders headers = new HttpHeaders();

    @Test
    public void getPrimeNumbersUpto10() throws JSONException {
        HttpEntity<PrimeNumbers> entity = new HttpEntity<PrimeNumbers>(null, headers);

        ResponseEntity<PrimeNumbers> response = restTemplate.exchange("http://localhost:1111/primes/10",
                HttpMethod.GET, entity, PrimeNumbers.class);
        PrimeNumbers result = response.getBody();

        JSONAssert.assertEquals("[2, 3, 5, 7]", result.getPrimes().toString(), false);
        JSONAssert.assertEquals("10", String.valueOf(result.getInitial()), false);
    }

}
