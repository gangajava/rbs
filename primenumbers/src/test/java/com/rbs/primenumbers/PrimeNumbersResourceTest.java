package com.rbs.primenumbers;

import static org.junit.Assert.assertEquals;

import com.rbs.primenumbers.exception.NoPrimeNumbersException;
import com.rbs.primenumbers.exception.NotValidInputException;
import com.rbs.primenumbers.model.PrimeNumbers;
import com.rbs.primenumbers.resource.PrimeNumbersResource;
import com.rbs.primenumbers.service.PrimeNumbersCache;
import com.rbs.primenumbers.service.PrimeNumbersService;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;

@RunWith(MockitoJUnitRunner.class)
public class PrimeNumbersResourceTest {
    private PrimeNumbersResource primeNumbersResource;
    @Mock
    private PrimeNumbersService primeNumbersService;

    @Before
    public void setUp() {
        primeNumbersResource = new PrimeNumbersResource(primeNumbersService);
    }

    @Test
    public void whenInputNumberIs_positiveInteger__thenReturnPrimeNumbers() {
        PrimeNumbers primes = new PrimeNumbers();
        List<Long> primeNumberList = new ArrayList<>();
        primeNumberList.add(2l);
        primeNumberList.add(3l);
        primeNumberList.add(5l);
        primeNumberList.add(7l);

        primes.setPrimes(primeNumberList);
        primes.setInitial(10);

        Mockito.when(primeNumbersService.getAllPrimeNumbers(10)).thenReturn(primes);
        HttpEntity<PrimeNumbers> primeNumbers = primeNumbersResource.getPrimeNumbers(10);
        assertEquals(4, primeNumbers.getBody().getPrimes().size());
    }

    @Test(expected=NotValidInputException.class)
    public void whenInputNumberIs_NegativeInteger__thenReturnException() {
        assertEquals(NotValidInputException.class,primeNumbersResource.getPrimeNumbers(-5l));
    }

    @Test(expected=NoPrimeNumbersException.class)
    public void whenInputNumberIs_One__thenReturnException() {
        assertEquals(NoPrimeNumbersException.class,primeNumbersResource.getPrimeNumbers(1));
    }
    @After
    public void tearDown() {

    }
}
