package com.rbs.primenumbers;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.rbs.primenumbers.model.PrimeNumbers;
import com.rbs.primenumbers.service.PrimeNumbersCache;
import com.rbs.primenumbers.service.PrimeNumbersService;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PrimeNumbersServiceTest {
    private PrimeNumbersService primeNumbersService;
    @Mock
    private PrimeNumbersCache primeNumbersCache;

    @Before
    public void setUp() {
        primeNumbersService = new PrimeNumbersService(primeNumbersCache);
    }

    @Test
    public void whenPrimes_Found_in_Cache__thenReturnPrimeNumbersFromCache() {
        PrimeNumbers primes = new PrimeNumbers();
        List<Long> primeNumberList = new ArrayList<>();
        primeNumberList.add(2l);
        primeNumberList.add(3l);
        primeNumberList.add(5l);
        primeNumberList.add(7l);

        primes.setPrimes(primeNumberList);
        primes.setInitial(10);

        Mockito.when(primeNumbersCache.getPrimeNumbersFronCache(10)).thenReturn(primes);
        PrimeNumbers primeNumbers = primeNumbersService.getAllPrimeNumbers(10);
        assertEquals(4, primeNumbers.getPrimes().size());
        verify(primeNumbersCache, times(0)).addPrimeNumbersToCache(anyLong(), any());
    }

    @Test
    public void whenPrimes_Not_Found_in_Cache__thenAddPrimeNumbersToCache() {
        Mockito.when(primeNumbersCache.getPrimeNumbersFronCache(10)).thenReturn(null);
        PrimeNumbers primeNumbers = primeNumbersService.getAllPrimeNumbers(10);
        assertEquals(4, primeNumbers.getPrimes().size());
        verify(primeNumbersCache, times(1)).addPrimeNumbersToCache(anyLong(), any());
    }
    @After
    public void tearDown() {

    }
}
